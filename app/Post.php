<?php

namespace Blog;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Post extends Eloquent
{
    use SoftDeletes;

    protected $fillable = [
        '_id',
        'title',
        'content',
        'description',
        'coverurl',
        'created_at',
        'updated_at',
    ];

    protected $connection = 'mongodb';

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getCoverUrl()
    {
        return $this->coverurl;
    }

    public function getDescription()
    {
        return $this->description;
    }
}
