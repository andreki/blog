@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            {{--Status Alert--}}
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            {{--Posts Listing--}}
            @forelse ($posts as $post)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h4><a href="/post/{{  $post->getId() }}">
                                {{ $post->getTitle() }}
                            </a></h4>
                        </div>
                        <div class="pull-right">
                            @auth
                                <form action="/post/{{ $post->getId() }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button class="btn btn-danger">@lang('posts.delete')</button>
                                </form>
                            @endauth
                        </div>
                        <div class="clearfix"></div>

                        @if($post->getCoverUrl())
                            <a href="/post/{{ $post->getId() }}"><img src="{{$post->getCoverUrl()}}" class="post-cover"/></a>
                        @endif
                    </div>
                    <div class="panel-body post-description">
                        {{$post->getDescription()}}
                    </div>
                </div>

            @empty
                @auth
                    <div class="alert alert-success">
                        <a href="{{route('post', [
                            'id' => "",
                        ])}}">@lang('posts.create_message')</a>
                    </div>
                @endauth
                <div class="alert alert-warning">
                    @lang('posts.no_posts')
                </div>
            @endforelse

            {{--Pagination Navigator--}}
            {{$posts->render()}}

        </div>
    </div>
</div>
@endsection
