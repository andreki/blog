@extends('layouts.app')

@section('content')
<div class="container">


    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            {{--Alerts--}}
            <div class="form-group">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <form action="/post/{{ $post->getId() }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field($post->getId()?"PATCH":"POST") }}

                    {{--Labels--}}
                    <div class="form-group">
                        <div class="pull-right">
                            @if($post->created_at)
                                <span class="label label-success post-labels">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                    @lang('common.created') {{$post->created_at}}
                                </span>
                            @endif
                            @if($post->updated_at)
                                <span class="label label-primary post-labels">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    @lang('common.updated') {{$post->updated_at}}
                                </span>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    {{--Heading--}}
                    <div class="panel-heading">
                        @auth
                            <label>
                                @lang('posts.title')
                                <input name="title" class="form-control form-control-lg" type="text" placeholder="@lang('posts.title_placeholder')" value="{{$post->getTitle() ?? old('title')}}"/>
                            </label>
                            <button type="submit" class="btn btn-success pull-right">
                                @lang('posts.save')
                            </button>
                        @else
                            @if($post->getCoverUrl())
                                <a href="/post/{{ $post->getId() }}"><img src="{{$post->getCoverUrl() ?? old('coverurl')}}" class="post-cover"/></a>
                            @endif
                            <h1>{{$post->getTitle()}}</h1>
                        @endauth
                    </div>

                    <div class="panel-body">
                        @auth
                            <div class="form-group">
                                <label>
                                    @lang('posts.description')
                                </label>
                                <textarea class='form-control' name="description" placeholder="@lang('posts.description_placeholder')">{{$post->getDescription() ?? old('description')}}</textarea>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <label>
                                    @lang('posts.content')
                                </label>
                                <textarea hidden id="ckeditor-content" name="content">{{$post->getContent() ?? old('content')}}</textarea>
                                <script>
                                    window.onload = function() {
                                        CKEDITOR.replace( 'ckeditor-content' );
                                    };
                                </script>
                            </div>

                            <label>
                                @lang('posts.coverurl')
                                <input name="coverurl" class="form-control" type="text" placeholder="@lang('posts.coverurl_placeholder')" value="{{$post->getCoverUrl() ?? old('coverurl')}}"/>
                            </label>
                        @else
                            <div>{!! $post->getContent() !!}</div>
                        @endauth
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
@endsection
